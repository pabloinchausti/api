//version inicial

var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var bodyParser = require('body-parser')
app.use(bodyParser.json())
app.use(function(req, res, next) {
  res.header("Access-Control-Allow-Origin", "*");
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
})

var requestjson = require('request-json');

var path = require('path');

var urlmovimientosMLab = "https://api.mlab.com/api/1/databases/eballesterosf/collections/movimientos?apiKey=GOLqWa850qO8tsdCUdby6eq9eKPInBkt";

var clienteMLab = requestjson.createClient(urlmovimientosMLab);

//importo el json en la variable
movimientosJSONV2=require('./movimientosv2.json')
app.listen(port);

console.log('todo list RESTful API server started on: ' + port);

app.get('/', function(req, res) {
  res.sendFile(path.join(__dirname, 'index.html'));
});

//le especifico que me van a pasar un parametro llamado idcliente
app.get('/clientes/:idcliente', function(req, res) {
  //para recuperar el parametro
  res.send('Aqui tiene al cliente número ' + req.params.idcliente);
});

app.get('/v1/movimientos', function(req, res) {
  res.sendFile(path.join(__dirname, 'movimientosv1.json'));
});

app.post('/', function(req, res) {
  res.send('Hola recibido su peticion post cambiada');
});

app.get('/v2/movimientos', function(req, res) {
  //lo devuelvo desde una variable
  res.send(movimientosJSONV2);
});

app.get('/v2/movimientos/:id', function(req, res) {
  //devuelvo un elemento especifico
  res.send(movimientosJSONV2[req.params.id]);
});

app.get('/v2/movimientosq', function(req, res) {
  //para enviar parametros por query
  console.log(req.query);
  res.send();
});

app.post('/v2/movimientos', (req, res) => {
  const nuevo = req.body;
  console.log(nuevo)
  nuevo.id = movimientosJSONV2.length ++;
  movimientosJSONV2.push(nuevo);
  res.status(200).send("Se agregó el elemento:" + JSON.stringify(nuevo) + "\n EN \n"  +  JSON.stringify(movimientosJSONV2));
})